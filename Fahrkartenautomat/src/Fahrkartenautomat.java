﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       while (true) {
       Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag); 
       } 
    }
    
    public static double fahrkartenbestellungErfassen() {
        Scanner erfassungsScan = new Scanner(System.in);
        int[] ticketChoiceArray = new int[10]; 
        double[] ticketPreis = new double[10]; 
        double zuZahlenderBetrag = 0;
        
        int ticketChoice = 0; 
        while (ticketChoiceArray[ticketChoice] < 1 || ticketChoiceArray[ticketChoice] > 1) {
	        System.out.println("Fahrkartenbestellvorgang:\r\n"
	    			+ "=========================\r\n"
	    			+ "\r\n"
	    			+ "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\r\n"
	    			+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
	    			+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
	    			+ "  Tageskarte Regeltarif AB [8,80 EUR] (2)\r\n"
	    			+ "  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)"
	    			+ "  Bezahlen (9)\n\n"
	    			+ "Ihre Wahl:");
	        
	        ticketChoice = erfassungsScan.nextInt(); 
	        if ((ticketChoice < 1 || ticketChoice > 3) && ticketChoice != 9) {
	        	System.out.println("Bitte verwenden Sie die vorgegebenen Zahlen, vielen Dank!");
	        	continue; 
	        } else if (ticketChoice == 1) {
	        	ticketPreis += 3; 
	        } else if (ticketChoice == 2) {
	        	ticketPreis += 8.80;
	        } else if (ticketChoice == 3) {
	        	ticketPreis += 25.50; 
	        } else if (ticketChoice == 9) {
	        	break; 
	        }
	        
	        
	        
	        System.out.println("Zwischensumme: " + ticketPreis + " € ");
	        
         
        
       
        
        System.out.println("Anzahl der Tickets: ");
        int ticketAnzahl = erfassungsScan.nextInt();
        while (ticketAnzahl <= 0 || ticketAnzahl > 10) {
        	System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        	System.out.println("Anzahl der Tickets: ");
        	ticketAnzahl = erfassungsScan.nextInt();
        }	
        
        zuZahlenderBetrag = ticketPreis * (double)ticketAnzahl; 
      
        }
        return zuZahlenderBetrag; 
    }
    public static double fahrkartenBezahlen (double zuZahlen) {
    	 Scanner tastatur = new Scanner(System.in);
    	 double eingezahlterGesamtbetrag = 0.0;
         while(eingezahlterGesamtbetrag < zuZahlen) {
        	 System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
        	 System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        	 double  eingeworfeneMünze = tastatur.nextDouble();
        	 eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
         return eingezahlterGesamtbetrag -= zuZahlen; 
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
     	   warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	 double rückgabebetrag = rueckgeld; 
    		       if(rückgabebetrag > 0.0) {
    		    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag );
    		    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    		           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    		           {
    		        	  System.out.println("2 EURO");
    			          rückgabebetrag -= 2.0;
    		           }
    		           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    		           {
    		        	  System.out.println("1 EURO");
    			          rückgabebetrag -= 1.0;
    		           }
    		           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    		           {
    		        	  System.out.println("50 CENT");
    			          rückgabebetrag -= 0.5;
    		           }
    		           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    		           {
    		        	  System.out.println("20 CENT");
    		 	          rückgabebetrag -= 0.2;
    		           }
    		           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    		           {
    		        	  System.out.println("10 CENT");
    			          rückgabebetrag -= 0.1;
    		           }
    		           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    		           {
    		        	  System.out.println("5 CENT");
    		 	          rückgabebetrag -= 0.05;
    		           }
    		       }

    		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    		                          "vor Fahrtantritt entwerten zu lassen!\n"+
    		                          "Wir wünschen Ihnen eine gute Fahrt.");
    		    }
    public static void warte(int millisekunde) {
    	System.out.print("=");
  	   try {
  		   Thread.sleep(millisekunde);
  	   } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void muenzeAusgeben(int rückgabeBetrag) {
    	System.out.println(
    		);
    }
}
