import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl; 
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent; 
	private int androidenAnzahl;
	private String schiffsname; 
	private ArrayList<String> broadcastKommunikator; 
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>(); 
	
	public Raumschiff() {
		
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}

	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	
	
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle(" -=*Click*=-");
		} else {
			this.photonentorpedoAnzahl -= 1;
			nachrichtAnAlle(" Photonentorpedo abgeschossen");
			treffer(r); 
		}
		
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			System.out.println(" -=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle(" Phaserkanone abgeschossen"); 
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		System.out.printf(" %s wurde getroffen!\n", this.schiffsname);
		this.schildeInProzent -= 50;
		if (this.schildeInProzent <= 0) {
			this.huelleInProzent -= 50; 
			this.energieversorgungInProzent -=50;
			if (this.huelleInProzent <= 0) {
				this.lebenserhaltungssystemInProzent = 0; 
				nachrichtAnAlle("Die Lebenserhaltungssysteme der " + r.getSchiffsname() + " wurden vollständig zerstört!");
			}
		}
	}
	
	
	
	public ArrayList<String> logbuchEintraegeZurueckgeben() {
		return getBroadcastKommunikator(); 		
	}
	
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}
	
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator; 
	} 
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		setPhotonentorpedoAnzahl(getAndroidenAnzahl() + anzahlTorpedos);
	}
	
	public void reperaturDurchfuehren (boolean schutzschilde, boolean energierversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
	}
	
	public void zustandRaumschiff() {
		System.out.printf(" Name des Schiffes: %s\n Anzahl Photonentorpedos: %d\n Energieversorung in Prozent: %d\n Schildaufladung in Prozent: %d\n "
				+ "Huelle in Prozent: %d\n Lebenserhaltungssystem in Prozent: %d\n Androidenanzahl: %d\n", this.schiffsname, this.photonentorpedoAnzahl, this.energieversorgungInProzent,
				this.schildeInProzent, this.huelleInProzent, this.lebenserhaltungssystemInProzent, this.androidenAnzahl);
	}
	
	
	
	public void ladungsausgeben() {
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++)
		System.out.println(" Ladungsname: " + this.ladungsverzeichnis.get(i).getBezeichnung() + " Ladungsmenge: " + this.ladungsverzeichnis.get(i).getMenge());
		
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		this.ladungsverzeichnis.clear();
	}

	@Override
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
				+ energieversorgungInProzent + ", schildeInProzent=" + schildeInProzent + ", huelleInProzent="
				+ huelleInProzent + ", lebenserhaltungssystemInProzent=" + lebenserhaltungssystemInProzent
				+ ", androidenAnzahl=" + androidenAnzahl + ", schiffsname=" + schiffsname + ", broadcastKommunikator="
				+ broadcastKommunikator + ", ladungsverzeichnis=" + ladungsverzeichnis + "]";
	}
}
