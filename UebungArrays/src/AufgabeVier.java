// 3, 7, 12, 18, 37 und 42


public class AufgabeVier {
	public static void main(String[] args) {
		int[] array = new int[6];
		array[0] = 3;
		array[1] = 7;
		array[2] = 12;
		array[3] = 18;
		array[4] = 37;
		array[5] = 42;
		boolean isTwelveInvolved = false;
		boolean isThirteenInvolved = false;
		System.out.print("[");
		for (int i = 0; i < array.length; i++) {
			System.out.print("  " + array[i]);
		}
		System.out.print("  ]\n");
		for (int i = 0; i < array.length; i++) {
			
			
			if (array[i] == 12) {
				isTwelveInvolved = true;
			} 
			if (array[i] == 13) {
				isThirteenInvolved = true;
			}
			
		}
		System.out.println(isTwelveInvolved ? "Die Zahl Zw�lf ist in der Ziehung enthalten" : "Die Zahl Zw�lf ist nicht in der Ziehung enthalten");
		System.out.println(isThirteenInvolved ? "Die Zahl Dreizehn ist in der Ziehung enthalten" : "Die Zahl Dreizehn ist nicht in der Ziehung enthalten");
	}
}


