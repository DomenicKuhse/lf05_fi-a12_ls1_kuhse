import java.util.Scanner;
public class NameAndAge {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Hello user! Please enter your name first!");
		String userName = sc.nextLine();
		System.out.println("Now enter your age as a number please!");
		int userAge = sc.nextInt();
		System.out.println("Your name is: " + userName + " and you are " + userAge + " years old");

	}

}
