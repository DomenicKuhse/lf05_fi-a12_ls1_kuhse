import java.util.Scanner; 
public class Konsoleneingabe
{

	public static void main(String[] args) {
	 
	//Addition
 	 Scanner myScanner = new Scanner(System.in);
	 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
	 int zahl1 = myScanner.nextInt();
	 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	 int zahl2 = myScanner.nextInt();
	 int ergebnis = zahl1 + zahl2;
	 System.out.print("\n\n\nErgebnis der Addition lautet: ");
	 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
	 //Subtraktion
	
	 int ergebnisSubtraktion = zahl1 - zahl2;
	 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
	 System.out.print(zahl1 + " - " + zahl2 +  " = " + ergebnisSubtraktion);
	 // Multiplikation
	 int ergebnisMultiplikation =  zahl1 * zahl2;
	 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
	 System.out.print(zahl1 + " * " + zahl2 +  " = " + ergebnisMultiplikation);
	 // Division
	 double ergebnisDivision = (double)zahl1 / zahl2;
	 System.out.print("\n\n\nErgebnis der Division lautet: ");
	 System.out.print(zahl1 + " : " + zahl2 +  " = " + ergebnisDivision);
	 
	 
	 myScanner.close();
	}
}